package solid.principle.interfce.segregation;

public interface ConvertCharToString {
    public void charToString();
}
