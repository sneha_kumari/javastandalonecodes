package solid.principle.interfce.segregation;

public interface ConvertIntToChar {
    public void intToChar();
}
