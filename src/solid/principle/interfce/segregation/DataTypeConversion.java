package solid.principle.interfce.segregation;

public class DataTypeConversion implements ConvertIntToDouble, ConvertCharToString
{
    public void intToDouble()
    {
//conversion logic
    }
    public void charToString()
    {
//conversion logic
    }
}
