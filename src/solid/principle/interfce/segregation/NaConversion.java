package solid.principle.interfce.segregation;

public interface NaConversion
{
    public void intToDouble();
    public void intToChar();
    public void charToString();
}