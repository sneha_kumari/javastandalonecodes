package solid.principle.open.close;

public abstract class Animal {
    public abstract String sound();
}

