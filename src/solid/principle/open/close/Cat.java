package solid.principle.open.close;

public class Cat extends Animal{

    public String sound() {
        return "meow";
    }
}
