package solid.principle.dependency.inversion.principle;

public interface Filter {
    void filter();
}
