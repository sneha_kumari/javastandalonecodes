package solid.principle.dependency.inversion.principle;

public class Client {
    public static void main(String[] args) {
        CoffeeMaker coffeeMaker = new CoffeeMaker(CoffeeFactory.filter("ThinFilter"));
        coffeeMaker.makeCoffee();
    }
}
