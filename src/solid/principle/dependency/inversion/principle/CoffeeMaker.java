package solid.principle.dependency.inversion.principle;

public class CoffeeMaker {
    private Filter bigFilter;

    public CoffeeMaker(Filter filter) {
        this.bigFilter = filter;
    }

    public void makeCoffee() {
        // steps
        bigFilter.filter();
        // steps
    }
}
