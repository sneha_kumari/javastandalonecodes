package solid.principle.liskov.substitution.notlsp;

public class LSPClient {

    public static void main(String[] args) {
        Rectangle r = new Square() ;
        r.setBreadth(10);
        r.setLength(20);
        System.out.println(r.getArea());
    }
}
