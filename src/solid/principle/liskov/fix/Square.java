package solid.principle.liskov.fix;


//derived classes must be completely substitutable for their base classes

public class Square {
    private Rectangle rectangle;

    public void setLength(int length) {
        rectangle.setLength(length);
        rectangle.setBreadth(length);
    }

    public int getLength() {
        return rectangle.getBreadth(); // return rectangle.getLength();
    }

    public int getArea() {
        return rectangle.getArea();
    }
}
