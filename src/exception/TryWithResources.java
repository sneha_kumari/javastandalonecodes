package exception;


import java.io.File;
import java.io.FileInputStream;

public class TryWithResources {
    public static void main(String[] args) {
        File file = new File("src/exception/arbit.txt");
        try(FileInputStream fileInputStream = new FileInputStream(file)) {
            throwSomeException();
            fileInputStream.read();
        } catch (Exception ignored) {
            System.out.println("ere");
        }
    }

    private static void throwSomeException() {
        throw new RuntimeException();
    }
}