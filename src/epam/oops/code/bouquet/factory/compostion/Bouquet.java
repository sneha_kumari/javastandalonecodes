package epam.oops.code.bouquet.factory.compostion;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public final class Bouquet {

    private final List<Flower> flowers = new ArrayList<>();

    public void addFlower(Flower flower){
        flowers.add(flower);
    }

    public int cost(){
        int cost = 0;
        for(Flower flr : flowers){
            cost = cost + flr.getCost();
        }
        return cost;
    }
}
