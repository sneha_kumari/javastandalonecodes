package constructor;

public class Child extends Parent {

    public Child() {
        super(13);
        System.out.println("I am child");
    }

    public static void main(String[] args) {
        Child childClass = new Child();
    }
}