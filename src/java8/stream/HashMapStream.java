package java8.stream;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class HashMapStream {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        System.out.println(set.stream().map(input -> input * 2).collect(Collectors.toSet()));
        // stream of values
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("sneha", "kumari");
        hashMap.put("harshit", "bangar");
//        hashMap.entrySet()
        Set<Map.Entry<String, String>> set1 = hashMap.entrySet();
        hashMap.keySet().stream().forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });
//        Consumer c = new Consumer<String>() {
//            @Override
//            public void accept(String s) {
//                System.out.println(s);
//            }
//        };
//        for (String s : hashMap.keySet()) {
//            c.accept(s);
//        }
        set1.stream().map(new Function<Map.Entry<String, String>, String>() {
            @Override
            public String apply(Map.Entry<String, String> entry) {
                return entry.getValue();
            }
        }).collect(Collectors.toList());
        for (Map.Entry<String, String> entry : hashMap.entrySet()) {
            System.out.println(entry.getValue());
        }
    }
}
