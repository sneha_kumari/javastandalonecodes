package java8.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class LambdaForEach {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(4, 5);

        // classic way
        for (int i : list) {
            System.out.println(i);
        }

        // using Anonymous class

        list.forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println(integer);
            }
        });

        // lambda style -> here Consumer's accept method is used
        list.forEach(
                (i) -> System.out.println(i)
        );

        // through method references
        // Method reference is used to refer to a method of functional interface.
        // It is a compact and easy form of lambda expression.
        // Each time when you are using lambda expression to just refer to a method, you can replace your lambda
        // expression with method reference.

        // When we are executing lambda exp with no input argument and executing a method with no argor we are
        // taking an arg and passing the same within the executing method -> Method Reference
        list.forEach(System.out::println);

    }
}