package java8.lambda.method.reference.instance;

interface Sayable{
    void say();
}