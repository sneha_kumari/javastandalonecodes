package java8.lambda.method.reference.constructor;

interface Messageable{
        Message getMessage(String msg);
        }