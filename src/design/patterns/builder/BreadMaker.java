package design.patterns.builder;

public class BreadMaker {
    public static void main(String[] args) {
        BreadComposition milkBread = new BreadComposition.Builder()
                .setHeight(1)
                .setMilkbread(true)
                .setCarb(10)
                .build();
        System.out.println(milkBread);
    }
}
